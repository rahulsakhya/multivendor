<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
           [ 'name' => 'Admin',
            'email'=>'admin@gmail.com',
            'password'=>Hash::make('admin'),
            'role'=>'admin',
            'status'=>'active'
          ],

          [
            'name' => 'Vendor',
            'email'=>'vendor@gmail.com',
            'password'=>Hash::make('vendor'),
            'role'=>'vendor',
            'status'=>'active'
          ],

           [
            'name' => 'Customer',
            'email'=>'user@gmail.com',
            'password'=>Hash::make('user'),
            'role'=>'user',
            'status'=>'active',
          ],
          ]);
    }
}
